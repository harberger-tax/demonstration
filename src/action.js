import {randomFloat, randomInt, round} from './util';

export const ActionType = {
  Purchase: 'Purchase',
  None: 'None',
};

class Action {
  constructor(type) {
    this.type = type;
  }
}

export class NoneAction extends Action {
  constructor() {
    super(ActionType.None);
  }
}

export class PurchaseAction extends Action {
  constructor(buyerId, sellerId) {
    super(ActionType.Purchase);
    this.buyerId = buyerId;
    this.sellerId = sellerId;
  }

  setPrice(price) {
    this.price = price;
  }
}

export const generateAction = ({id, funds, chance}, participants, currentSeller) => {
  if (funds < 1 || chance < 0.01 || currentSeller === id || randomFloat() >= chance) {
    return new NoneAction();
  }

  if (currentSeller !== null) {
    return new PurchaseAction(id, currentSeller);
  }

  const sellers = participants.filter((seller) => seller.id !== id && seller.chunks > 0);
  if (sellers.length < 1) {
    return new NoneAction();
  }

  const sellerId = sellers.length > 1 ? randomInt(0, sellers.length - 1) : sellers[0].id;
  return new PurchaseAction(id, sellerId);
};

export const generateBid = ({funds, chance}, price) => {
  if (funds < price) {
    return 0;
  }

  return round(Math.min(price + randomFloat() * chance, funds));
};
