import Vue from 'vue';
import LoadScript from 'vue-plugin-load-script';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import App from './App.vue';
import router from './router';
import store from './store';

Vue.use(LoadScript);
Vue.use(BootstrapVue);

Vue.config.productionTip = false;

Vue.filter('round', (value) => {
  if (value % 1 === 0) {
    return value;
  }
  return value.toFixed(2);
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
