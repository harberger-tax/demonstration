import Vue from 'vue';
import VueRouter from 'vue-router';
import {Root, Start, Run} from '../views';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Root',
    component: Root,
    children: [
      {
        path: 'start',
        name: 'Start',
        component: Start,
      },
      {
        path: 'run',
        name: 'Run',
        component: Run,
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.path === '/') {
    next({name: 'Start'});
  } else {
    next();
  }
});

router.afterEach((to) => {
  document.title = to.name;
});

export default router;
