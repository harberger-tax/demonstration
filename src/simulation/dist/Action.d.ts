export declare enum ActionType {
    Purchase = "PURCHASE",
    Reward = "REWARD",
    None = "NONE"
}
export interface Transaction {
    buyer: number;
    seller: number;
}
export declare class Action {
    type: ActionType;
    chance: number;
    symmetric: boolean;
    constructor(type: ActionType, chance: number);
    setSymmetric(state?: boolean): void;
    toJSON(): {
        type: ActionType;
        chance: number;
    };
    toString(delimiter?: string): string;
}
export declare class RewardAction extends Action {
    constructor();
}
export declare class NoneAction extends Action {
    constructor(chance: number);
}
export declare class PurchaseAction extends Action {
    transactions: Transaction[];
    constructor(transactions: Transaction[], chance: number);
    toJSON(): {
        type: ActionType;
        chance: number;
        transactions: Transaction[];
    };
    toString(delimiter?: string): string;
}
//# sourceMappingURL=Action.d.ts.map