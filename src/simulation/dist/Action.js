"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseAction = exports.NoneAction = exports.RewardAction = exports.Action = exports.ActionType = void 0;
const util_1 = require("./util");
var ActionType;
(function (ActionType) {
    ActionType["Purchase"] = "PURCHASE";
    ActionType["Reward"] = "REWARD";
    ActionType["None"] = "NONE";
})(ActionType = exports.ActionType || (exports.ActionType = {}));
class Action {
    constructor(type, chance) {
        this.symmetric = false;
        this.type = type;
        this.chance = util_1.round(chance);
    }
    setSymmetric(state = true) {
        this.symmetric = state;
    }
    toJSON() {
        const { type, chance } = this;
        return { type, chance };
    }
    toString(delimiter = '<br>') {
        const chance = `chance: ${this.chance}`;
        const symmetric = this.symmetric ? `${delimiter}symmetric` : '';
        return `${chance}${delimiter}${this.type.valueOf()}${symmetric}`;
    }
}
exports.Action = Action;
class RewardAction extends Action {
    constructor() {
        super(ActionType.Reward, 1);
    }
}
exports.RewardAction = RewardAction;
class NoneAction extends Action {
    constructor(chance) {
        super(ActionType.None, chance);
    }
}
exports.NoneAction = NoneAction;
class PurchaseAction extends Action {
    constructor(transactions, chance) {
        super(ActionType.Purchase, chance);
        this.transactions = transactions;
    }
    toJSON() {
        const { type, chance, transactions } = this;
        return { type, chance, transactions };
    }
    toString(delimiter = '<br>') {
        const { transactions } = this;
        const chance = `chance: ${this.chance}`;
        const symmetric = this.symmetric ? `${delimiter}(symmetric)` : '';
        const summary = transactions
            .map(({ buyer, seller }) => `P${buyer} PURCHASE P${seller}`)
            .join(delimiter);
        return `${chance}${delimiter}${summary}${symmetric}`;
    }
}
exports.PurchaseAction = PurchaseAction;
//# sourceMappingURL=Action.js.map