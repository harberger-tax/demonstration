export declare type ObjectMap<T> = {
    [id: string]: T;
};
export declare type RecursiveMap<T> = {
    [id: string]: T | RecursiveMap<T>;
};
export declare class Registry<T> {
    contents: ObjectMap<T>;
    constructor(contents?: ObjectMap<T>);
    keys(): string[];
    set(id: string, value: T): void;
    has(id: string): boolean;
    get(id: string): T;
    reduce(reducer: (newContents: ObjectMap<T>, id: string, oldContents: ObjectMap<T>) => ObjectMap<T>): void;
}
//# sourceMappingURL=Map.d.ts.map