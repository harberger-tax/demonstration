"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Registry = void 0;
class Registry {
    constructor(contents) {
        this.contents = {};
        if (contents) {
            Object.keys(contents).forEach((id) => {
                this.set(id, contents[id]);
            });
        }
    }
    keys() {
        return Object.keys(this.contents);
    }
    set(id, value) {
        this.contents[id] = value;
    }
    has(id) {
        return !!this.contents[id];
    }
    get(id) {
        return this.contents[id];
    }
    reduce(reducer) {
        this.contents = this.keys().reduce((newContents, id) => reducer(newContents, id, this.contents), {});
    }
}
exports.Registry = Registry;
//# sourceMappingURL=Map.js.map