import { Participant } from './Participant';
import { IO } from './IO';
import { Action, Transaction } from './Action';
import { ObjectMap, Registry } from './Map';
export interface SerialisedState {
    id: string;
    depth: number;
    chance: number;
    participants: {
        id: number;
        funds: number;
        chunks: number;
    }[];
    states: {
        [id: string]: string[];
    };
}
interface StateParams {
    depth: number;
    chance: number;
    action?: Action;
}
interface StateOptions {
    maxDepth: number;
    tax: number;
    discountFactor: number;
    symmetric: boolean;
    single: boolean;
}
export declare class State {
    id: string;
    participants: Participant[];
    states: ObjectMap<Action>;
    duplicatedStates: ObjectMap<Action>;
    visited: boolean;
    io: IO;
    params: StateParams;
    options: StateOptions;
    constructor(io: IO, participants: Participant[], params: StateParams, options: StateOptions);
    applyAction(action: Action): void;
    generateTransactions(): Transaction[][];
    generateActions(): Action[];
    generateStates(registry: Registry<State>): void;
    visitStates(registry: Registry<State>): string[];
    semiEquals(state: State): boolean;
    equals(state: State): boolean;
    toJSON(): SerialisedState;
}
export declare const visit: (registry: Registry<State>, id: string) => void;
export interface Path {
    probability: number;
    steps: number;
    subPaths?: ObjectMap<Path | string>;
}
export declare const findPath: (registry: Registry<State>, rootId: string, targetId: string, history?: string[], probability?: number, steps?: number) => Path | null;
export {};
//# sourceMappingURL=State.d.ts.map