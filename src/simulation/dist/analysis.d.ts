import { Registry } from './Map';
import { State } from './State';
import { Participant } from './Participant';
declare type Id = {
    id: string;
};
declare type Count = {
    count: number;
};
declare type Value = {
    value: number;
};
declare class ParticipantAnalysis {
    id: number;
    best: Id & Count & Value;
    worst: Id & Count & Value;
    totalStatesWithinThreshold: number;
    debug: boolean;
    constructor(debug: boolean, id: number, best?: Id & Count & Value, worst?: Id & Count & Value, totalStatesWithinThreshold?: number);
    toJSON(): {
        id: number;
        best: Id & Count & Value;
        worst: Id & Count & Value;
        totalStatesWithinThreshold: number;
    };
    toString(): string;
    csvHeaders(): string;
    toCSV(): string;
}
export declare type AnalysisResult = {
    mostEqual: Id & Value;
    leastEqual: Id & Value;
    totalEndStates: number;
    totalStatesWithinThreshold: number;
    participants: ParticipantAnalysis[];
};
export declare class Analysis {
    registry: Registry<State>;
    participants: Participant[];
    analyses: {
        [depth: number]: AnalysisResult;
    };
    debug: boolean;
    constructor(registry: Registry<State>, participants: Participant[], options: {
        debug: boolean;
    });
    analyse(options: {
        depth?: number;
        wealthDifference: number;
        maxDepth: number;
        mergeRewards: boolean;
    }): AnalysisResult[];
    toJSON(): AnalysisResult[];
    toString(): string;
    csvHeaders(): string;
    toCSV(): string[];
}
export {};
//# sourceMappingURL=analysis.d.ts.map