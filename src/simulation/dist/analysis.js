"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Analysis = void 0;
class ParticipantAnalysis {
    constructor(debug, id, best, worst, totalStatesWithinThreshold) {
        this.debug = debug;
        this.id = id;
        this.best = best || { id: '', count: 0, value: 0 };
        this.worst = worst || { id: '', count: 0, value: Infinity };
        this.totalStatesWithinThreshold = totalStatesWithinThreshold || 0;
    }
    toJSON() {
        const { id, best, worst, totalStatesWithinThreshold } = this;
        return {
            id,
            best,
            worst,
            totalStatesWithinThreshold,
        };
    }
    toString() {
        const best = `best: ${this.best.value}`;
        const worst = `worst: ${this.worst.value}`;
        const totalStatesWithinThreshold = `total end states within threshold: ${this.totalStatesWithinThreshold}`;
        return `participant ${this.id} - ${[best, worst, totalStatesWithinThreshold].join(', ')}`;
    }
    csvHeaders() {
        return [
            'id',
            this.debug && 'best.id',
            'best.count',
            'best.value',
            this.debug && 'worst.id',
            'worst.count',
            'worst.value',
            'totalStatesWithinThreshold',
        ]
            .filter((el) => el !== false)
            .join(',');
    }
    toCSV() {
        const { id, best, worst, totalStatesWithinThreshold } = this;
        return [
            id,
            this.debug && best.id,
            best.count,
            best.value,
            this.debug && worst.id,
            worst.count,
            worst.value,
            totalStatesWithinThreshold,
        ]
            .filter((el) => el !== false)
            .join(',');
    }
}
class Analysis {
    constructor(registry, participants, options) {
        this.registry = registry;
        this.participants = participants;
        this.analyses = {};
        this.debug = options.debug;
    }
    analyse(options) {
        const { wealthDifference } = Object.assign({}, options);
        const { registry } = this;
        const fundDiff = (participants) => {
            let min = Infinity;
            let max = 0;
            participants.forEach(({ funds }) => {
                if (min > funds) {
                    min = funds;
                }
                else if (max < funds) {
                    max = funds;
                }
            });
            return Math.abs(max - min);
        };
        registry.keys().forEach((id) => {
            const state = registry.get(id);
            const { depth } = state.params;
            const { participants } = state;
            if (!this.analyses[depth]) {
                this.analyses[depth] = {
                    mostEqual: { id: '', value: 0 },
                    leastEqual: { id: '', value: Infinity },
                    totalStatesWithinThreshold: 0,
                    totalEndStates: 0,
                    participants: participants.map(({ id }) => new ParticipantAnalysis(this.debug, id)),
                };
            }
            const analysis = this.analyses[depth];
            const diff = fundDiff(participants);
            analysis.totalEndStates += 1;
            if (!analysis.mostEqual.id || diff < analysis.mostEqual.value) {
                analysis.mostEqual = { id, value: diff };
            }
            if (!analysis.leastEqual.id || diff > analysis.leastEqual.value) {
                analysis.leastEqual = { id, value: diff };
            }
            if (diff < wealthDifference) {
                analysis.totalStatesWithinThreshold += 1;
            }
            participants.forEach((participant, i) => {
                const previousAnalysis = analysis.participants[i];
                if (participant.funds > previousAnalysis.best.value) {
                    previousAnalysis.best = { id, count: 1, value: participant.funds };
                }
                else if (participant.funds === previousAnalysis.best.value) {
                    previousAnalysis.best.count += 1;
                }
                else if (participant.funds < previousAnalysis.worst.value) {
                    previousAnalysis.worst = { id, count: 1, value: participant.funds };
                }
                else if (participant.funds === previousAnalysis.worst.value) {
                    previousAnalysis.worst.count += 1;
                }
            });
        });
        return Object.values(this.analyses);
    }
    toJSON() {
        return Object.values(this.analyses);
    }
    toString() {
        const analysis = this.analyses[Object.values(this.analyses).length - 1];
        const mostEqual = `most equal state: ${analysis.mostEqual.value}`;
        const leastEqual = `least equal state: ${analysis.leastEqual.value}`;
        const totalStates = `total end states: ${analysis.totalEndStates}`;
        const totalStatesWithinThreshold = `total end states within threshold: ${analysis.totalStatesWithinThreshold}`;
        const participants = analysis.participants
            .map((participant) => participant.toString())
            .join('\n');
        return [mostEqual, leastEqual, totalStates, totalStatesWithinThreshold, participants].join('\n');
    }
    csvHeaders() {
        const participantHeaders = this.analyses[0].participants.map((p) => p.csvHeaders()).join(',');
        return [
            this.debug && 'mostEqual.id',
            'mostEqual.value',
            this.debug && 'leastEqual.id',
            'leastEqual.value',
            'totalEndStates',
            'totalStatesWithinThreshold',
            participantHeaders,
        ]
            .filter((el) => el !== false)
            .join(',');
    }
    toCSV() {
        return Object.values(this.analyses).map(({ mostEqual, leastEqual, totalEndStates, totalStatesWithinThreshold, participants }) => [
            this.debug && mostEqual.id,
            mostEqual.value,
            this.debug && leastEqual.id,
            leastEqual.value,
            totalEndStates,
            totalStatesWithinThreshold,
            participants.map((p) => p.toCSV()).join(','),
        ]
            .filter((el) => el !== false)
            .join(','));
    }
}
exports.Analysis = Analysis;
//# sourceMappingURL=analysis.js.map