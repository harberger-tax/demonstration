"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("source-map-support/register");
const IO_1 = require("./IO");
const Participant_1 = require("./Participant");
const State_1 = require("./State");
const yargs_1 = __importDefault(require("yargs"));
const visualise_1 = require("./visualise");
const Map_1 = require("./Map");
const util_1 = require("./util");
const analysis_1 = require("./analysis");
const args = yargs_1.default
    .option('format', {
    describe: 'output format',
    choices: visualise_1.formats,
    nargs: 1,
    default: 'md',
})
    .option('output', {
    describe: 'output filename (extension will be based on format)',
    type: 'string',
    nargs: 1,
    default: '',
})
    .option('max-depth', {
    describe: 'maximum depth to generate',
    type: 'number',
    default: Infinity,
})
    .option('debug', {
    describe: 'enable debugging info',
    type: 'boolean',
    default: false,
    nargs: 0,
})
    .option('merge-rewards', {
    describe: 'merge reward states into regular states',
    type: 'boolean',
    default: false,
    nargs: 0,
})
    .option('tax', {
    describe: 'Amount of taxation to apply per reward, as percentage',
    type: 'number',
    default: 0,
})
    .option('discount-factor', {
    describe: 'Depreciation rate of reward, as percentage',
    type: 'number',
    default: 0,
})
    .option('wealth-difference', {
    describe: 'Maximum difference between wealth of participants',
    type: 'number',
    default: Infinity,
})
    .option('symmetric', {
    describe: 'Omit symmetric states (p1 state equivalent to p2)',
    type: 'boolean',
    default: false,
    nargs: 0,
})
    .option('single', {
    describe: 'Randomly select a single action to generate a tree for instead of all actions',
    type: 'boolean',
    default: false,
    nargs: 0,
})
    .help()
    .alias('h', 'help').argv;
const { debug, output, tax, symmetric, single, 'merge-rewards': mergeRewards, 'max-depth': tempMaxDepth, 'wealth-difference': wealthDifference, 'discount-factor': discountFactor, } = args;
const start = Date.now();
console.log(new Date().toLocaleTimeString());
let depth = 0;
if (discountFactor) {
    while (true) {
        const discount = (1 / (1 + discountFactor)) ** depth;
        const rate = (1 - tax) * discount;
        if (rate < 0.009) {
            break;
        }
        depth++;
    }
}
const maxDepth = tempMaxDepth && discountFactor && depth < tempMaxDepth
    ? depth
    : tempMaxDepth
        ? tempMaxDepth
        : 2000;
const format = args.format;
const filename = output ? `${output}-${maxDepth}.${format}` : output;
console.log({
    debug,
    filename,
    tax,
    discountFactor,
    wealthDifference,
    mergeRewards,
    symmetric,
    maxDepth,
    single,
});
const io = new IO_1.IO({ debug: args.debug, filename });
const participants = [
    new Participant_1.Participant(0, 0, 2, 0),
    new Participant_1.Participant(1, 1, 0, () => 0.9),
    new Participant_1.Participant(2, 1, 0, 0.9),
];
const registry = new Map_1.Registry();
const time = {};
const state = new State_1.State(io, participants, { depth: 0, chance: 1 }, { maxDepth, tax, discountFactor, symmetric, single });
registry.set(state.id, state);
state.generateStates(registry);
State_1.visit(registry, state.id);
time.simulation = util_1.duration(start);
console.log('simulation took', time.simulation, 's');
const analysis = new analysis_1.Analysis(registry, participants, { debug });
const analysisResult = analysis.analyse({
    wealthDifference,
    maxDepth,
    mergeRewards,
});
time.analysis = util_1.duration(start - time.simulation);
console.log('analysis took', time.analysis, 's');
const data = visualise_1.stringifyState(registry, { debug, mergeRewards, format });
time.stringify = util_1.duration(start - time.analysis);
console.log('stringify took', time.stringify, 's');
if (format === 'csv') {
    const meta = { maxDepth, tax, discountFactor, wealthDifference };
    io.file(Object.keys(meta).join(',') + '\n');
    io.file(Object.values(meta).join(',') + '\n');
    io.file(`depth,${analysis.csvHeaders()}\n`);
    analysis.toCSV().forEach((chunk, i) => io.file(`${i},${chunk}\n`));
    const stateIO = new IO_1.IO({ debug, filename: `states.txt` });
    data.forEach((chunk) => stateIO.file(chunk));
    time.serialization = util_1.duration(start - time.stringify);
    console.log('serialization took', time.serialization, 's');
}
else {
    const { styles, meta } = visualise_1.visualise(participants.slice(0, 2), state, registry, analysisResult[analysisResult.length - 1], {
        debug,
        format,
        mergeRewards,
    });
    const serialized = visualise_1.toFormat(format, data.join('\n'), styles, meta);
    time.visualisation = util_1.duration(start - time.stringify);
    console.log('visualisation took', time.visualisation, 's');
    if (filename) {
        io.file(serialized);
    }
    else {
        io.output(serialized);
    }
    time.serialization = util_1.duration(start - time.visualisation);
    console.log('serialization took', time.visualisation, 's');
}
time.total = util_1.duration(start);
console.log('total time was', time.total, 's');
console.log(new Date().toLocaleTimeString());
console.log(analysis.toString());
//# sourceMappingURL=index.js.map