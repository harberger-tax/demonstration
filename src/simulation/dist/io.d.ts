export declare class IO {
    private debug;
    private filename?;
    constructor(options: {
        debug?: boolean;
        filename?: string;
    });
    log(message: string, ...args: any[]): this;
    output(payload: string): this;
    file(payload: string): this | undefined;
}
export declare class StreamIO {
    private indentation;
    private stream;
    private writes;
    constructor(filename: string);
    indent(): void;
    outdent(): void;
    write(data: string): Promise<void>;
    start(): Promise<void>;
    end(): Promise<void>;
}
//# sourceMappingURL=IO.d.ts.map