"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StreamIO = exports.IO = void 0;
const fs = __importStar(require("fs"));
const fullWidth = 10;
const DEBUG = process.env.DEBUG || false;
const pad = (message, max = fullWidth) => {
    let padding = '';
    let amount = max - message.length;
    while (padding.length < amount) {
        padding += ' ';
    }
    return message + padding;
};
const format = (payload, indentation = 0) => {
    const indent = pad('', indentation);
    const data = payload.split('\n').join(`\n${indent}`);
    return `${indent}${data}`;
};
class IO {
    constructor(options) {
        const { debug, filename } = options;
        this.debug = debug || false;
        this.filename = filename;
        if (filename && fs.existsSync(filename)) {
            fs.unlinkSync(filename);
        }
    }
    log(message, ...args) {
        if (this.debug) {
            const payload = [pad(message), ...args];
            if (this.filename) {
                this.file(`<!-- ${payload.join(' ')} -->\n`);
            }
            else {
                console.log(...payload);
            }
        }
        return this;
    }
    output(payload) {
        const data = format(payload);
        console.log(data);
        return this;
    }
    file(payload) {
        if (!this.filename) {
            console.log('no filename set, not writing');
            return;
        }
        const data = format(payload);
        fs.appendFileSync(this.filename, data);
        return this;
    }
}
exports.IO = IO;
class StreamIO {
    constructor(filename) {
        if (fs.existsSync(filename)) {
            fs.unlinkSync(filename);
        }
        this.indentation = 0;
        this.stream = fs.createWriteStream(filename);
        this.writes = [];
    }
    indent() {
        this.indentation += 1;
    }
    outdent() {
        this.indentation -= 1;
    }
    async write(data) {
        const indented = data.replace(/\n/g, '\n' + pad('', this.indentation * 2));
        await Promise.all(this.writes);
        const writePromise = new Promise((resolve, reject) => {
            this.stream.write(indented, (err) => {
                if (err) {
                    reject(err);
                }
                resolve();
            });
        });
        this.writes.push(writePromise);
        await writePromise;
    }
    async start() {
        await this.write('[');
        this.indent();
    }
    async end() {
        this.outdent();
        await this.write(']');
        await Promise.all(this.writes);
        this.stream.close();
    }
}
exports.StreamIO = StreamIO;
//# sourceMappingURL=IO.js.map