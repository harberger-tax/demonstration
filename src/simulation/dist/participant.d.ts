export declare type ChanceFnParams = {
    depth: number;
    participants: Participant[];
};
export declare type ChanceFn = (params: ChanceFnParams) => number;
export interface Participant {
    id: number;
    funds: number;
    chunks: number;
    chance: number;
}
export declare class Participant implements Participant {
    chanceFn: ChanceFn | null;
    constructor(id: number, funds: number, chunks: number, chance: ChanceFn | number);
    generateChance(params: ChanceFnParams): void;
    clone(): Participant;
    equals(other: Participant): boolean;
    semiEquals(other: Participant): boolean;
    toJSON(): {
        id: number;
        funds: number;
        chunks: number;
    };
    toString(): string;
}
//# sourceMappingURL=Participant.d.ts.map