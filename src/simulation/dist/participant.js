"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Participant = void 0;
class Participant {
    constructor(id, funds, chunks, chance) {
        this.id = id;
        this.funds = funds;
        this.chunks = chunks;
        this.chance = typeof chance === 'number' ? chance : 0;
        this.chanceFn = typeof chance !== 'number' ? chance : null;
    }
    generateChance(params) {
        this.chance = this.chanceFn ? this.chanceFn(params) : this.chance;
    }
    clone() {
        return new Participant(this.id, this.funds, this.chunks, this.chanceFn || this.chance);
    }
    equals(other) {
        return other.id === this.id && this.semiEquals(other);
    }
    semiEquals(other) {
        return this.funds === other.funds && this.chunks === other.chunks;
    }
    toJSON() {
        return {
            id: this.id,
            funds: this.funds,
            chunks: this.chunks,
        };
    }
    toString() {
        return `P${this.id}: ${this.funds}, ${this.chunks}`;
    }
}
exports.Participant = Participant;
//# sourceMappingURL=Participant.js.map