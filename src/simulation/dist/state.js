"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findPath = exports.visit = exports.State = void 0;
const Action_1 = require("./Action");
const util_1 = require("./util");
let symmetric = 0;
class State {
    constructor(io, participants, params, options) {
        var _a;
        this.visited = false;
        this.io = io;
        this.params = params;
        this.options = options;
        this.participants = participants.map((participant) => participant.clone());
        this.participants.forEach((participant) => participant.generateChance(Object.assign(Object.assign({}, this.params), { participants: this.participants })));
        this.states = {};
        this.duplicatedStates = {};
        if (this.params.action) {
            this.applyAction(this.params.action);
        }
        this.id =
            this.participants.map(({ id, funds, chunks }) => `I${id}F${funds}C${chunks}`).join('-') +
                (this.params.depth % 2 !== 0 ? 'R' : '');
        this.io.log('NEW', this.id, this.params.depth, '[', ((_a = this.params.action) === null || _a === void 0 ? void 0 : _a.toString(', ')) || 'NONE', ']');
    }
    applyAction(action) {
        switch (action.type) {
            case Action_1.ActionType.Reward:
                this.participants.forEach((participant) => {
                    const tax = 1 - this.options.tax;
                    const discount = (1 / (1 + this.options.discountFactor)) ** this.params.depth;
                    const funds = participant.funds + participant.chunks * tax * discount;
                    const rounded = Number(funds.toFixed(2));
                    participant.funds = rounded;
                });
                break;
            case Action_1.ActionType.Purchase:
                action.transactions.forEach(({ buyer, seller }) => {
                    this.participants[buyer].funds = util_1.round(this.participants[buyer].funds - 1);
                    this.participants[buyer].chunks += 1;
                    this.participants[seller].funds = util_1.round(this.participants[seller].funds + 1);
                    this.participants[seller].chunks -= 1;
                });
                break;
            case Action_1.ActionType.None:
                break;
            default:
                console.log('unhandled action', action);
        }
    }
    generateTransactions() {
        const sellers = this.participants.filter((participant) => participant.chunks > 0);
        const buyers = this.participants.filter(({ id, chance, funds }) => id !== 0 && chance > 0 && funds >= 1);
        if (buyers.length < 1) {
            return [];
        }
        const transactionSets = [];
        buyers.forEach(({ id: buyer }) => {
            const result = [];
            sellers.forEach(({ id: seller }) => {
                if (seller !== buyer) {
                    result.push({ buyer, seller });
                }
            });
            if (result.length > 0) {
                transactionSets.push(result);
            }
        });
        const result = [];
        transactionSets.forEach((set, i) => {
            set.forEach((transaction) => {
                transactionSets.slice(i + 1).forEach((otherSet) => {
                    otherSet.forEach((otherTransaction) => {
                        if (transaction.seller !== otherTransaction.seller ||
                            this.participants[transaction.seller].chunks > 1) {
                            result.push([transaction, otherTransaction]);
                        }
                    });
                });
                result.push([transaction]);
            });
        });
        return result;
    }
    generateActions() {
        if (this.id.endsWith('R')) {
            return [new Action_1.RewardAction()];
        }
        const transactionSets = this.generateTransactions();
        const sellers = this.participants.filter((participant) => participant.chunks > 0);
        const buyers = this.participants.filter(({ id, chance, funds }) => id !== 0 && chance > 0 && funds >= 1);
        const sets = transactionSets.map((set) => {
            const chances = buyers.map(({ id, chance }) => {
                if (set.length === 1 && set[0].seller === id) {
                    return 1;
                }
                const participating = set.find(({ buyer }) => buyer === id);
                const otherSellers = sellers.filter((seller) => id !== seller.id);
                return participating ? chance / otherSellers.length : 1 - chance;
            });
            const total = chances.reduce((total, chance) => total * chance);
            return new Action_1.PurchaseAction(set, total);
        });
        const noneChance = buyers.reduce((total, { chance }) => total * (1 - chance), 1);
        const result = [...sets, new Action_1.NoneAction(noneChance)];
        if (result.length === 1) {
            result[0].chance = 1;
        }
        if (this.options.single) {
            const chances = result.reduce((chances, { chance }) => {
                chances.push(chances.length < 1 ? chance : chances[chances.length - 1] + chance);
                return chances;
            }, []);
            const roll = util_1.randomFloat(0, 1);
            const action = chances.findIndex((chance) => chance >= roll);
            return [action > -1 ? result[action] : result[result.length - 1]];
        }
        return result;
    }
    generateStates(registry) {
        if (this.params.depth >= this.options.maxDepth) {
            return;
        }
        const actions = this.generateActions();
        if (actions.length < 1) {
            return;
        }
        actions.forEach((action) => {
            const state = new State(this.io, this.participants, Object.assign(Object.assign({}, this.params), { chance: action.chance * this.params.chance, depth: this.params.depth + 1, action }), this.options);
            const [p0, p1, p2] = state.id.replace(/I\d/g, '').split('-');
            const symmetricId = `I0${p0}-I1${p2}-I2${p1}`;
            if (this.options.symmetric && registry.has(symmetricId)) {
                action.setSymmetric();
                this.duplicatedStates[symmetricId] = action;
                symmetric += 1;
            }
            else if (registry.has(state.id)) {
                this.duplicatedStates[state.id] = action;
            }
            else {
                this.states[state.id] = action;
                registry.set(state.id, state);
            }
        });
        this.io.log('GENERATE', this.id, '[', Object.keys(this.states).join(', '), ']');
    }
    visitStates(registry) {
        if (!this.visited) {
            this.visited = true;
        }
        return Object.keys(this.states).reduce((states, id) => {
            if (registry.get(id).visited) {
                return states;
            }
            this.io.log('VISIT', this.id, '=>', id);
            registry.get(id).generateStates(registry);
            states.push(id);
            return states;
        }, []);
    }
    semiEquals(state) {
        const [, a1, a2] = this.participants;
        const [, b1, b2] = state.participants;
        return (a1.semiEquals(b2) && a2.semiEquals(b1)) || (a1.semiEquals(b1) && a2.semiEquals(b2));
    }
    equals(state) {
        return this.id === state.id && this.semiEquals(state);
    }
    toJSON() {
        const participants = this.participants.map((participant) => participant.toJSON());
        const states = {};
        const add = (from) => Object.keys(from).forEach((key) => {
            if (!states[key])
                states[key] = [];
            states[key].push(from[key].toString());
        });
        add(this.states);
        add(this.duplicatedStates);
        return {
            id: this.id,
            depth: this.params.depth,
            chance: util_1.roundFuzzy(this.params.chance),
            participants,
            states,
        };
    }
}
exports.State = State;
exports.visit = (registry, id) => {
    let queue = registry.get(id).visitStates(registry);
    registry.get(id).visited = true;
    while (queue.length > 0) {
        const next = [];
        queue.forEach((subId) => {
            const more = registry.get(subId).visitStates(registry);
            registry.get(subId).visited = true;
            next.push(...more);
        });
        queue = next;
    }
    console.log('symmetric states omitted', symmetric);
};
exports.findPath = (registry, rootId, targetId, history = [], probability = 1, steps = 0) => {
    if (history.includes(rootId)) {
        return null;
    }
    if (rootId.includes(targetId)) {
        return { probability, steps };
    }
    const subStateIds = Object.keys(registry.get(rootId).states);
    if (subStateIds.length < 1) {
        return null;
    }
    const results = {};
    const subPaths = subStateIds.reduce((paths, id) => {
        const subPath = exports.findPath(registry, id, targetId, [...history, rootId], util_1.roundFuzzy(probability * registry.get(rootId).states[id].chance), steps + 1);
        if (subPath) {
            paths[id] = subPath;
            results.probability = subPath.probability;
            results.steps = subPath.steps;
        }
        return paths;
    }, {});
    if (subPaths && Object.keys(subPaths).length > 0) {
        return Object.assign({ probability, steps, subPaths }, results);
    }
    return null;
};
//# sourceMappingURL=State.js.map