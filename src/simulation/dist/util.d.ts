export declare const rainbow: (numOfSteps: number) => (step: number) => string;
export declare const round: (value: number, places?: number) => number;
export declare const roundFuzzy: (value: number, sigFigs?: number) => number;
export declare const duration: (from: number) => number;
export declare const randomFloat: (floor: number, ceil: number) => number;
export declare const random: (floor: number, ceil: number) => number;
//# sourceMappingURL=util.d.ts.map