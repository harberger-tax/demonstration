"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.random = exports.randomFloat = exports.duration = exports.roundFuzzy = exports.round = exports.rainbow = void 0;
exports.rainbow = (numOfSteps) => (step) => {
    const h = step / numOfSteps;
    const i = ~~(h * 6);
    const f = h * 6 - i;
    const q = 1 - f;
    const [r, g, b] = (() => {
        switch (i % 6) {
            case 0:
                return [1, f, 0];
            case 1:
                return [q, 1, 0];
            case 2:
                return [0, 1, f];
            case 3:
                return [0, q, 1];
            case 4:
                return [f, 0, 1];
            case 5:
                return [1, 0, q];
            default:
                return [0, 0, 0];
        }
    })();
    return ('#' +
        ('00' + (~~(r * 255)).toString(16)).slice(-2) +
        ('00' + (~~(g * 255)).toString(16)).slice(-2) +
        ('00' + (~~(b * 255)).toString(16)).slice(-2));
};
exports.round = (value, places = 2) => Number((Math.round(value * 10 ** places) / 10 ** places).toFixed(places));
exports.roundFuzzy = (value, sigFigs = 2) => {
    const str = value.toString();
    if (str.includes('e')) {
        return Number(str.replace(/(\d+(\.[1-9]+)?)(\.?\d+)(e.*)/, '$1$4'));
    }
    const first = str.match(/[1-9]/);
    if (!first || !first.index) {
        return value;
    }
    return Number(str.slice(0, first.index + sigFigs));
};
exports.duration = (from) => exports.round((Date.now() - from) / 1000);
exports.randomFloat = (floor, ceil) => exports.round(Math.random() * (ceil - floor) + floor);
exports.random = (floor, ceil) => Math.ceil(exports.randomFloat(floor, ceil));
//# sourceMappingURL=util.js.map