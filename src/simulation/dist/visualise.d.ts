import { State } from './State';
import { Participant } from './Participant';
import { Registry } from './Map';
import { AnalysisResult } from './analysis';
export declare type Format = 'txt' | 'md' | 'html' | 'csv';
export declare const formats: string[];
export declare const stringifyState: (registry: Registry<State>, { debug, mergeRewards, format, }: {
    debug: boolean;
    mergeRewards: boolean;
    format: Format;
}) => string[];
export declare const visualise: (participants: Participant[], root: State, registry: Registry<State>, analysis: AnalysisResult, { debug, mergeRewards, format, }: {
    debug: boolean;
    mergeRewards: boolean;
    format: Format;
}) => {
    styles: string[];
    meta: {
        color?: string | undefined;
        content: string;
    }[];
};
export declare const toFormat: (format: Format, data: string, styles: string[], meta: {
    color?: string;
    content: string;
}[]) => string;
//# sourceMappingURL=visualise.d.ts.map