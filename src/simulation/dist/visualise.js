"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toFormat = exports.visualise = exports.stringifyState = exports.formats = void 0;
const State_1 = require("./State");
const util_1 = require("./util");
exports.formats = ['txt', 'md', 'html', 'csv'];
exports.stringifyState = (registry, { debug = false, mergeRewards = true, format = 'txt', }) => {
    const delim = format === 'txt' ? ' - ' : '<br>';
    const transistionMap = {};
    const withoutR = (id) => (mergeRewards ? id.replace(/R$/, '') : id);
    let states = 0;
    let transitions = 0;
    registry.keys().forEach((id) => {
        const state = registry.get(id);
        const participants = state.participants
            .map(({ id, funds, chunks }) => `P${id}, ${funds}, ${chunks}`)
            .join(delim);
        const actualId = withoutR(state.id);
        const chance = `chance: ${util_1.roundFuzzy(state.params.chance)}`;
        const depth = `depth: ${state.params.depth}`;
        const title = debug ? [actualId, depth, chance].join(delim) : chance;
        const declaration = `${actualId}[${title}${delim}${participants}]`;
        if (!transistionMap[declaration]) {
            transistionMap[declaration] = [];
            states += 1;
        }
        const add = (from) => Object.keys(from).forEach((id) => {
            const actualSubId = withoutR(id);
            transistionMap[declaration].push(`${actualId} --> |${from[id].toString()}| ${actualSubId}`);
            transitions += 1;
        });
        add(state.states);
        add(state.duplicatedStates);
    });
    let result = [];
    Object.entries(transistionMap).forEach(([declaration, transitions]) => result.push(`${declaration}\n${transitions.join('\n')}`));
    console.log('states', states);
    console.log('transitions', transitions);
    return result;
};
exports.visualise = (participants, root, registry, analysis, { debug = false, mergeRewards = false, format = 'html', }) => {
    const getRandomColor = util_1.rainbow(participants.length * 3 + 2);
    const colors = participants.reduce((colors, { id }) => [
        ...colors,
        getRandomColor(id * participants.length),
        getRandomColor(id * participants.length + 1),
        getRandomColor(id * participants.length + 2),
    ], []);
    colors.push(getRandomColor(participants.length * 3 + 1));
    const withoutR = (id) => id.replace(/R$/, '');
    if (mergeRewards) {
        registry.reduce((newContents, id, oldContents) => {
            const state = oldContents[id];
            const newState = newContents[withoutR(id)];
            if (newState) {
                Object.keys(state.states).forEach((id) => {
                    newState.states[withoutR(id)] = state.states[id];
                });
            }
            else {
                state.states = Object.keys(state.states).reduce((newStates, id) => {
                    newStates[withoutR(id)] = state.states[id];
                    return newStates;
                }, {});
                newContents[withoutR(id)] = state;
            }
            return newContents;
        });
    }
    const meta = [{ content: `number of states: ${registry.keys().length}` }];
    const styles = [
        '.label foreignObject { overflow: visible; }',
        '.edgePath path { stroke: black; }',
        '.node rect { fill: white; stroke: black; }',
    ];
    const colored = {};
    const addNodeStyle = (pid, id) => {
        const label = `#${id} rect`;
        colored[label] = `{ stroke: ${colors[pid]} }`;
    };
    const addEdgeStyle = (pid, from, to) => {
        const label = `.edgePath[id*="${from}"][id*="${to}"] path`;
        colored[label] = `{ stroke: ${colors[pid]} }`;
    };
    const walkPath = (pid, root, path) => {
        if (!path || !path.subPaths) {
            return;
        }
        const { subPaths } = path;
        Object.keys(subPaths).forEach((id) => {
            const subPath = subPaths[id];
            if (typeof subPath === 'string') {
                return;
            }
            walkPath(pid, id, subPath);
            addEdgeStyle(pid, root, id);
        });
    };
    const { mostEqual, leastEqual } = analysis;
    addNodeStyle(colors.length - 1, leastEqual.id);
    addNodeStyle(colors.length - 1, mostEqual.id);
    const leastEqualPath = State_1.findPath(registry, root.id, leastEqual.id);
    const mostEqualPath = State_1.findPath(registry, root.id, mostEqual.id);
    walkPath(colors.length - 1, root.id, leastEqualPath);
    walkPath(colors.length - 1, root.id, mostEqualPath);
    meta.push(leastEqualPath
        ? {
            content: `least equal state funds difference: ${util_1.round(leastEqual.value)}, steps: ${leastEqualPath.steps}, ${leastEqualPath.probability} ${debug ? leastEqual.id : ''}`,
        }
        : { content: 'least equal path not found' });
    meta.push(mostEqualPath
        ? {
            content: `most equal state funds difference: ${util_1.round(mostEqual.value)}, steps: ${mostEqualPath.steps}, ${mostEqualPath.probability} ${debug ? mostEqual.id : ''}`,
        }
        : { content: 'most equal path not found' });
    participants.forEach(({ id }, index) => {
        const { best, worst } = analysis.participants[id];
        meta.push({
            color: colors[participants.length + index],
            content: `participant ${id} - best last funds: ${util_1.round(best.value)}${debug ? ` ${best.id}` : ''}`,
        });
        addNodeStyle(participants.length + index, best.id);
        const bestPath = State_1.findPath(registry, root.id, best.id);
        walkPath(participants.length + index, root.id, bestPath);
        meta.push({
            color: colors[2 * participants.length + index],
            content: `participant ${id} - worst last funds: ${util_1.round(worst.value)}${debug ? ` ${worst.id}` : ''}`,
        });
        addNodeStyle(participants.length + index, worst.id);
        const worstPath = State_1.findPath(registry, root.id, worst.id);
        walkPath(participants.length + index, root.id, worstPath);
    });
    Object.entries(colored).forEach(([selector, rules]) => styles.push(`${selector} ${rules}`));
    return { styles, meta };
};
exports.toFormat = (format, data, styles, meta) => {
    const mermaidOptions = JSON.stringify({
        startOnLoad: true,
        maxTextSize: 1000000000,
        themeCSS: styles.join(' '),
        flowchart: {
            diagramPadding: 2,
            useMaxWidth: false,
            curve: 'basis',
        },
    });
    const metadata = meta
        .map(({ color, content }) => `<span ${color ? `style="color: ${color}"` : ''}>${content}</span>`)
        .join('<br>');
    const output = {
        html: `<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"></head><body>` +
            `<div class="meta">${metadata}</div>` +
            `<div class="mermaid">graph LR\n${data}</div>` +
            `<script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>` +
            `<script>mermaid.initialize(${mermaidOptions});</script>` +
            `</body></html>`,
        md: `\`\`\`mermaid\ngraph TD\n${data}\n\`\`\``,
        txt: data,
        csv: '',
    };
    return output[format];
};
//# sourceMappingURL=visualise.js.map