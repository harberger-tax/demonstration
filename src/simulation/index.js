import {State, visit} from './dist/State';
import {IO} from './dist/IO';
import {Registry} from './dist/Map';
import {Analysis} from './dist/analysis';
import {visualise, stringifyState} from './dist/visualise';
import {Participant} from './dist/Participant';

export const generateTree = (
  initParticipants,
  {discountFactor, tax, maxDepth, wealthDifference},
) => {
  const io = new IO({});
  const registry = new Registry();
  const mergeRewards = true;
  const symmetric = false;
  const format = 'html';
  const participants = initParticipants.map(({id, funds, chunks, chance}) => {
    return new Participant(id, funds, chunks, chance);
  });

  const state = new State(
    io,
    participants,
    {depth: 0, chance: 1},
    {maxDepth, tax, discountFactor, symmetric},
  );
  registry.set(state.id, state);
  state.generateStates(registry);
  visit(registry, state.id);
  const analysisResult = new Analysis(registry, participants, {debug: false}).analyse({
    wealthDifference,
    maxDepth,
    mergeRewards,
  });
  const {styles, meta} = visualise(
    participants,
    state,
    registry,
    analysisResult[analysisResult.length - 1],
    {
      format,
      mergeRewards,
    },
  );
  const data = stringifyState(registry, {mergeRewards, format});
  console.log({styles, meta, data});
  return {analysisResult, data, styles, meta};
};
