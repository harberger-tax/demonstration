import Vue from 'vue';
import Vuex from 'vuex';
import {round} from '../util';

Vue.use(Vuex);

export const mutations = {
  REWARD: 'reward',
  FUNDS: 'funds',
  CHUNKS: 'chunks',
  CHANCE: 'chance',
  PARAMETERS: 'parameters',
  INCREMENTROUND: 'incrementRound',
};

export const actions = {
  TRANSACT: 'transact',
  SET: 'set',
  NEXTROUND: 'nextRound',
};

export const clone = ({state}) => JSON.parse(JSON.stringify(state));

export default new Vuex.Store({
  state: {
    participants: [
      {id: 0, name: 'Pool', funds: 0, chunks: 2, chance: 0},
      {id: 1, name: 'You', funds: 1, chunks: 0, chance: 0.9},
      {id: 2, name: 'Opponent', funds: 1, chunks: 0, chance: 0.9},
    ],
    tax: 0.01,
    discount: 0,
    round: 0,
  },
  getters: {},
  mutations: {
    reward(state) {
      const modifier = (1 - state.tax) * (1 / (1 + state.discount)) ** state.round;
      state.participants.forEach((participant) => {
        participant.funds = round(participant.funds + participant.chunks * modifier);
      });
    },
    funds(state, {id, value, change}) {
      if (change) {
        state.participants[id].funds += change;
      } else {
        state.participants[id].funds = value;
      }
    },
    chunks(state, {id, value, change}) {
      if (change) {
        state.participants[id].chunks += change;
      } else {
        state.participants[id].chunks = value;
      }
    },
    chance(state, {id, value}) {
      state.participants[id].chance = value;
    },
    parameters(state, {tax, discount}) {
      state.tax = tax;
      state.discount = discount;
    },
    incrementRound(state) {
      state.round += 1;
    },
  },
  actions: {
    transact({commit}, {buyerId, sellerId, price}) {
      commit(mutations.FUNDS, {id: buyerId, change: -price});
      commit(mutations.FUNDS, {id: sellerId, change: price});
      commit(mutations.CHUNKS, {id: buyerId, change: 1});
      commit(mutations.CHUNKS, {id: sellerId, change: -1});
    },
    set({commit}, {id, funds, chunks, chance}) {
      commit(mutations.FUNDS, {id, value: funds});
      commit(mutations.CHUNKS, {id, value: chunks});
      commit(mutations.CHANCE, {id, value: chance});
    },
    nextRound({commit}) {
      commit(mutations.REWARD);
      commit(mutations.INCREMENTROUND);
    },
  },
  modules: {},
});
