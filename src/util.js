export const random = (lower, upper) => Math.random() * (upper - lower + 1) + lower;

export const randomFloat = () => Math.random().toFixed(2);

export const randomInt = (lower, upper) => Math.floor(random(lower, upper));

export const round = (value) => (value % 1 === 0 ? value : Number(value.toFixed(2)));
